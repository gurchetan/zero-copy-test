# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
ZERO_COPY_TEST = zero_copy_test
SOURCES += zero_copy_test.c
OBJS = $(SOURCES:.c=.o)
DEPS = $(SOURCES:.c=.d)
PKG_CONFIG ?= pkg-config
CFLAGS += -O0 -ggdb3
CCFLAGS += $(shell $(PKG_CONFIG) --cflags vulkan egl glesv2) 
LDLIBS += $(PC_LIBS)
LDLIBS += $(shell $(PKG_CONFIG) --libs vulkan egl glesv2)
.PHONY: all clean
all: $(ZERO_COPY_TEST)
$(ZERO_COPY_TEST): $(OBJS)
clean:
	$(RM) $(ZERO_COPY_TEST)
	$(RM) $(OBJS) $(DEPS)
	$(RM) *.o *.d .version
$(ZERO_COPY_TEST):
	$(CC) $(CCFLAGS) $(CFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS)
$(OBJS): %.o: %.c
	$(CC) $(CCFLAGS) $(CFLAGS) -c $< -o $@ -MMD
-include $(DEPS)
