/*
 * Copyright 2021 Android Open Source Project
 * SPDX-License-Identifier: MIT
 */
#define _GNU_SOURCE

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vulkan/vulkan.h>

#if defined(__linux__)
#include <sys/ioctl.h>
#include <sys/syscall.h>

#include "memfd_util.h"
#include "udmabuf.h"
#elif defined(__Fuchsia__)
// add some defines here
#endif

#define BUFFER_SIZE (8 * getpagesize())

#define CHECK(cond)                                                                           \
	do {                                                                                  \
		if (!(cond)) {                                                                \
			printf("CHECK failed in %s() %s:%d\n", __func__, __FILE__, __LINE__); \
			return 0;                                                             \
		}                                                                             \
	} while (0)
#define HANDLE_EINTR(x)                                                  \
	({                                                               \
		int eintr_wrapper_counter = 0;                           \
		int eintr_wrapper_result;                                \
		do {                                                     \
			eintr_wrapper_result = (x);                      \
		} while (eintr_wrapper_result == -1 && errno == EINTR && \
			 eintr_wrapper_counter++ < 100);                 \
		eintr_wrapper_result;                                    \
	})
#define ARRAY_SIZE(A) (sizeof(A) / sizeof(*(A)))
#define check_vk_success(result, vk_func) \
	__check_vk_success(__FILE__, __LINE__, __func__, (result), (vk_func))

static void __check_vk_success(const char *file, int line, const char *func, VkResult result,
			       const char *vk_func)
{
	if (result == VK_SUCCESS)
		return;

	printf("ERROR: [%s - %s: %d]: %s failed with VkResult(%d)\n", func, file, line, vk_func,
	       result);
	exit(EXIT_FAILURE);
}

struct functions {
	PFN_vkGetMemoryFdPropertiesKHR vkGetMemoryFdPropertiesKHR;
	PFN_vkGetMemoryFdKHR vkGetMemoryFdKHR;

	PFNGLCREATEMEMORYOBJECTSEXTPROC glCreateMemoryObjectsEXT;
	PFNGLIMPORTMEMORYFDEXTPROC glImportMemoryFdEXT;
	PFNGLBUFFERSTORAGEMEMEXTPROC glBufferStorageMemEXT;
};

struct external_memory {
	int fd;
	bool created_by_vulkan;
};

struct zero_copy_test {
	struct external_memory ext_mem;
	VkDevice dev;
	VkPhysicalDevice phys_dev;
	struct functions funcs;

#if defined(__linux__)
	int udmabuf_dev;
#elif defined(__Fuchsia__)
#endif
};

const char *get_gl_error()
{
	switch (glGetError()) {
		case GL_NO_ERROR:
			return "No error has been recorded.";
		case GL_INVALID_ENUM:
			return "An unacceptable value is specified for an enumerated argument. The "
			       "offending command is ignored and has no other side effect than to "
			       "set the error flag.";
		case GL_INVALID_VALUE:
			return "A numeric argument is out of range. The offending command is "
			       "ignored and has no other side effect than to set the error flag.";
		case GL_INVALID_OPERATION:
			return "The specified operation is not allowed in the current state. The "
			       "offending command is ignored and has no other side effect than to "
			       "set the error flag.";
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			return "The command is trying to render to or read from the framebuffer "
			       "while the currently bound framebuffer is not framebuffer complete "
			       "(i.e. the return value from glCheckFramebufferStatus is not "
			       "GL_FRAMEBUFFER_COMPLETE). The offending command is ignored and has "
			       "no other side effect than to set the error flag.";
		case GL_OUT_OF_MEMORY:
			return "There is not enough memory left to execute the command. The state "
			       "of the GL is undefined, except for the state of the error flags, "
			       "after this error is recorded.";
		default:
			return "Unknown error";
	}
}

const char *get_egl_error()
{
	switch (eglGetError()) {
		case EGL_SUCCESS:
			return "The last function succeeded without error.";
		case EGL_NOT_INITIALIZED:
			return "EGL is not initialized, or could not be initialized, for the "
			       "specified EGL display connection.";
		case EGL_BAD_ACCESS:
			return "EGL cannot access a requested resource (for example a context is "
			       "bound in another thread).";
		case EGL_BAD_ALLOC:
			return "EGL failed to allocate resources for the requested operation.";
		case EGL_BAD_ATTRIBUTE:
			return "An unrecognized attribute or attribute value was passed in the "
			       "attribute list.";
		case EGL_BAD_CONTEXT:
			return "An EGLContext argument does not name a valid EGL rendering "
			       "context.";
		case EGL_BAD_CONFIG:
			return "An EGLConfig argument does not name a valid EGL frame buffer "
			       "configuration.";
		case EGL_BAD_CURRENT_SURFACE:
			return "The current surface of the calling thread is a window, pixel "
			       "buffer or pixmap that is no longer valid.";
		case EGL_BAD_DISPLAY:
			return "An EGLDisplay argument does not name a valid EGL display "
			       "connection.";
		case EGL_BAD_SURFACE:
			return "An EGLSurface argument does not name a valid surface (window, "
			       "pixel buffer or pixmap) configured for GL rendering.";
		case EGL_BAD_MATCH:
			return "Arguments are inconsistent (for example, a valid context requires "
			       "buffers not supplied by a valid surface).";
		case EGL_BAD_PARAMETER:
			return "One or more argument values are invalid.";
		case EGL_BAD_NATIVE_PIXMAP:
			return "A NativePixmapType argument does not refer to a valid native "
			       "pixmap.";
		case EGL_BAD_NATIVE_WINDOW:
			return "A NativeWindowType argument does not refer to a valid native "
			       "window.";
		case EGL_CONTEXT_LOST:
			return "A power management event has occurred. The application must "
			       "destroy all contexts and reinitialise OpenGL ES state and objects "
			       "to continue rendering.";
		default:
			return "Unknown error";
	}
}

static bool has_extension(const char *extension, const char *extensions)
{
	const char *start, *where, *terminator;
	start = extensions;
	for (;;) {
		where = (char *)strstr((const char *)start, extension);
		if (!where)
			break;
		terminator = where + strlen(extension);
		if (where == start || *(where - 1) == ' ')
			if (*terminator == ' ' || *terminator == '\0')
				return true;
		start = terminator;
	}
	return false;
}

static int find_memory_type(VkPhysicalDevice phys_dev, VkMemoryPropertyFlags properties,
			    uint32_t *index, int memory_type_bits)
{
	VkPhysicalDeviceMemoryProperties memory_props;
	vkGetPhysicalDeviceMemoryProperties(phys_dev, &memory_props);

	for (uint32_t i = 0; i < memory_props.memoryTypeCount; i++) {
		if ((memory_type_bits & (1 << i)) == 0)
			continue;

		if ((memory_props.memoryTypes[i].propertyFlags & properties) == properties) {
			*index = i;
			return 0;
		}
	}

	printf("failed to get properties");
	exit(EXIT_FAILURE);
	return 0;
}

// Choose a physical device that supports Vulkan 1.1 or later. Exit on failure.
static VkPhysicalDevice choose_physical_device(VkInstance inst)
{
	uint32_t n_phys_devs;
	VkResult res;

	res = vkEnumeratePhysicalDevices(inst, &n_phys_devs, NULL);
	check_vk_success(res, "vkEnumeratePhysicalDevices");

	if (n_phys_devs == 0) {
		fprintf(stderr, "No available VkPhysicalDevices\n");
		exit(EXIT_FAILURE);
	}

	VkPhysicalDevice phys_devs[n_phys_devs];
	res = vkEnumeratePhysicalDevices(inst, &n_phys_devs, phys_devs);
	check_vk_success(res, "vkEnumeratePhysicalDevices");

	// Print information about all available devices. This helps debugging
	// when bringing up Vulkan on a new system.
	printf("Available VkPhysicalDevices:\n");

	uint32_t physical_device_idx = 0;
	VkPhysicalDevice physical_device = VK_NULL_HANDLE;
	for (uint32_t i = 0; i < n_phys_devs; ++i) {
		VkPhysicalDeviceProperties props;

		vkGetPhysicalDeviceProperties(phys_devs[i], &props);

		printf("    VkPhysicalDevice %u:\n", i);
		printf("	apiVersion: %u.%u.%u\n", VK_VERSION_MAJOR(props.apiVersion),
		       VK_VERSION_MINOR(props.apiVersion), VK_VERSION_PATCH(props.apiVersion));
		printf("	driverVersion: %u\n", props.driverVersion);
		printf("	vendorID: 0x%x\n", props.vendorID);
		printf("	deviceID: 0x%x\n", props.deviceID);
		printf("	deviceName: %s\n", props.deviceName);
		printf("	pipelineCacheUUID: %x%x%x%x-%x%x-%x%x-%x%x-%x%x%x%x%x%x\n",
		       props.pipelineCacheUUID[0], props.pipelineCacheUUID[1],
		       props.pipelineCacheUUID[2], props.pipelineCacheUUID[3],
		       props.pipelineCacheUUID[4], props.pipelineCacheUUID[5],
		       props.pipelineCacheUUID[6], props.pipelineCacheUUID[7],
		       props.pipelineCacheUUID[8], props.pipelineCacheUUID[9],
		       props.pipelineCacheUUID[10], props.pipelineCacheUUID[11],
		       props.pipelineCacheUUID[12], props.pipelineCacheUUID[13],
		       props.pipelineCacheUUID[14], props.pipelineCacheUUID[15]);
		if (physical_device == VK_NULL_HANDLE && VK_VERSION_MAJOR(props.apiVersion) >= 1 &&
		    VK_VERSION_MINOR(props.apiVersion) >= 1) {
			physical_device_idx = i;
			physical_device = phys_devs[i];
		}
	}

	if (physical_device == VK_NULL_HANDLE) {
		printf("unable to find a suitable physical device");
		exit(EXIT_FAILURE);
	}

	printf("Chose VkPhysicalDevice %d\n", physical_device_idx);
	fflush(stdout);
	return physical_device;
}

// Return the index of a graphics-enabled queue family. Return UINT32_MAX on
// failure.
static uint32_t choose_gfx_queue_family(VkPhysicalDevice phys_dev)
{
	uint32_t family_idx = UINT32_MAX;
	VkQueueFamilyProperties *props = NULL;
	uint32_t n_props = 0;

	vkGetPhysicalDeviceQueueFamilyProperties(phys_dev, &n_props, NULL);

	props = calloc(sizeof(props[0]), n_props);
	if (!props) {
		printf("out of memory");
		exit(EXIT_FAILURE);
	}

	vkGetPhysicalDeviceQueueFamilyProperties(phys_dev, &n_props, props);

	// Choose the first graphics queue.
	for (uint32_t i = 0; i < n_props; ++i) {
		if ((props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) && props[i].queueCount > 0) {
			family_idx = i;
			break;
		}
	}

	free(props);
	return family_idx;
}

static int test_init_vk(struct zero_copy_test *test)
{
	VkInstance inst;
	VkResult res;
	uint32_t gfx_queue_family_idx;

	res = vkCreateInstance(
	    &(VkInstanceCreateInfo){
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo =
		    &(VkApplicationInfo){
			.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
			.apiVersion = VK_MAKE_VERSION(1, 1, 0),
		    },
	    },
	    /*pAllocator*/ NULL, &inst);

	check_vk_success(res, "vkCreateInstance");

	test->phys_dev = choose_physical_device(inst);
	gfx_queue_family_idx = choose_gfx_queue_family(test->phys_dev);
	if (gfx_queue_family_idx == UINT32_MAX) {
		printf(
		    "VkPhysicalDevice exposes no VkQueueFamilyProperties "
		    "with graphics");
		exit(EXIT_FAILURE);
	}

	const uint32_t num_required_extensions = 1;
	const char *required_extensions[] = {
		"VK_KHR_external_memory_fd",
	};

	uint32_t extension_count;
	vkEnumerateDeviceExtensionProperties(test->phys_dev, NULL, &extension_count, NULL);
	VkExtensionProperties available_extensions[extension_count];
	vkEnumerateDeviceExtensionProperties(test->phys_dev, NULL, &extension_count,
					     available_extensions);
	for (uint32_t i = 0; i < num_required_extensions; i++) {
		uint32_t j;
		for (j = 0; j < extension_count; j++) {
			if (strcmp(required_extensions[i], available_extensions[j].extensionName) ==
			    0) {
				break;
			}
		}
		if (j == extension_count) {
			printf("unsupported device extension: %s", required_extensions[i]);
			exit(EXIT_FAILURE);
		}
	}

	res = vkCreateDevice(test->phys_dev,
			     &(VkDeviceCreateInfo){
				 .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
				 .queueCreateInfoCount = 1,
				 .pQueueCreateInfos =
				     (VkDeviceQueueCreateInfo[]){
					 {
					     .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
					     .queueFamilyIndex = gfx_queue_family_idx,
					     .queueCount = 1,
					     .pQueuePriorities = (float[]){ 1.0f },

					 },
				     },
				 .enabledExtensionCount = num_required_extensions,
				 .ppEnabledExtensionNames = required_extensions,
			     },
			     /*pAllocator*/ NULL, &test->dev);

	check_vk_success(res, "vkCreateDevice");

	test->funcs.vkGetMemoryFdPropertiesKHR =
	    (void *)vkGetDeviceProcAddr(test->dev, "vkGetMemoryFdPropertiesKHR");

	test->funcs.vkGetMemoryFdKHR = (void *)vkGetDeviceProcAddr(test->dev, "vkGetMemoryFdKHR");

	return 0;
}

static int test_init_gl(struct zero_copy_test *test)
{
	EGLDisplay display;
	EGLContext ctx;

	display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	if (display == EGL_NO_DISPLAY) {
		printf("failed to get the display\n");
		return -EINVAL;
	}

	if (!eglInitialize(display, NULL, NULL)) {
		printf("egl initialize failed with error: %s\n", get_egl_error());
		return -EINVAL;
	}

	// Bind the API
	if (!eglBindAPI(EGL_OPENGL_ES_API)) {
		printf("failed to bind OpenGL ES: %s\n", get_egl_error());
		return -EINVAL;
	}

	// Get any EGLConfig. We need one to create a context, but it isn't used to create any
	// surfaces.
	const EGLint config_attribs[] = { EGL_RENDERABLE_TYPE,
					  EGL_OPENGL_ES2_BIT,
					  EGL_RED_SIZE,
					  8,
					  EGL_GREEN_SIZE,
					  8,
					  EGL_BLUE_SIZE,
					  8,
					  EGL_ALPHA_SIZE,
					  8,
					  EGL_DEPTH_SIZE,
					  0,
					  EGL_STENCIL_SIZE,
					  0,
					  EGL_NONE };

	EGLConfig egl_config;
	EGLint num_configs;
	if (!eglChooseConfig(display, config_attribs, &egl_config, 1,
			     &num_configs /* unused but can't be null */)) {
		printf("eglChooseConfig() failed with error: %s\n", get_egl_error());
		return -EINVAL;
	}

	const EGLint context_attribs[] = { EGL_CONTEXT_MAJOR_VERSION, 3, EGL_CONTEXT_MINOR_VERSION,
					   2, EGL_NONE };

	ctx = eglCreateContext(display, egl_config, EGL_NO_CONTEXT /* no shared context */,
			       context_attribs);
	if (ctx == EGL_NO_CONTEXT) {
		printf("failed to create OpenGL ES Context: %s\n", get_egl_error());
		return -EINVAL;
	}

	if (!eglMakeCurrent(display, EGL_NO_SURFACE /* no default draw surface */,
			    EGL_NO_SURFACE /* no default draw read */, ctx)) {
		printf("failed to make the OpenGL ES Context current: %s\n", get_egl_error());
		return -EINVAL;
	}

	const char *gl_extensions = (const char *)glGetString(GL_EXTENSIONS);
	if (!has_extension("GL_EXT_memory_object", gl_extensions)) {
		printf("GL_EXT_memory_object missing");
		return -EINVAL;
	}

	if (!has_extension("GL_EXT_memory_object_fd", gl_extensions)) {
		printf("GL_EXT_memory_object_fd missing");
		return -EINVAL;
	}

	test->funcs.glCreateMemoryObjectsEXT =
	    (PFNGLCREATEMEMORYOBJECTSEXTPROC)eglGetProcAddress("glCreateMemoryObjectsEXT");
	test->funcs.glImportMemoryFdEXT =
	    (PFNGLIMPORTMEMORYFDEXTPROC)eglGetProcAddress("glImportMemoryFdEXT");
	test->funcs.glBufferStorageMemEXT =
	    (PFNGLBUFFERSTORAGEMEMEXTPROC)eglGetProcAddress("glBufferStorageMemEXT");

	return 0;
}

static int test_init_platform_specific(struct zero_copy_test *test)
{
#if defined(__linux__)
	test->udmabuf_dev = HANDLE_EINTR(open("/dev/udmabuf", O_RDWR));
	if (test->udmabuf_dev < 0) {
		printf("failed to open udmabuf device: %s\n", strerror(errno));
		return -EINVAL;
	}
#elif defined(__Fuchsia__)
#endif
	return 0;
}

static int test_vk_create(struct zero_copy_test *test)
{
	VkResult res;
	VkDeviceMemory memory;
	int ret;
	uint8_t *addr;
	uint32_t index;

	ret = find_memory_type(
	    test->phys_dev,
	    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &index, ~0);
	if (ret < 0)
		return ret;

	const VkExportMemoryAllocateInfo export_info = {
		.sType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_KHR,
		.pNext = NULL,
		.handleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT,
	};

	res = vkAllocateMemory(test->dev,
			       &(VkMemoryAllocateInfo){
				   .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
				   .pNext = &export_info,
				   .allocationSize = BUFFER_SIZE,
				   .memoryTypeIndex = index,
			       },
			       /*pAllocator*/ NULL, &memory);

	check_vk_success(res, "vkAllocateMemory");

	res = vkMapMemory(test->dev, memory, 0, BUFFER_SIZE, 0, (void **)&addr);
	check_vk_success(res, "vkMapMemory");
	memset(addr, 6, BUFFER_SIZE);
	vkUnmapMemory(test->dev, memory);

	res = test->funcs.vkGetMemoryFdKHR(
	    test->dev,
	    &(VkMemoryGetFdInfoKHR){ .sType = VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR,
				     .memory = memory,
				     .handleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT },
	    &test->ext_mem.fd);
	check_vk_success(res, "vkGetMemoryFdKHR");
	vkFreeMemory(test->dev, memory, /*pAllocator*/ NULL);

	test->ext_mem.created_by_vulkan = true;
	return 0;
}

static int test_create_from_sglist(struct zero_copy_test *test)
{
	int ret;
#if defined(__linux__)
	uint8_t *addr;
	struct udmabuf_create create;

	if (test->udmabuf_dev < 0) {
		printf("udmabuf not present\n");
		return -EINVAL;
	}

	int memfd = memfd_create("zero_copy_test", MFD_ALLOW_SEALING);
	if (memfd < 0) {
		printf("memfd_create() error: %s", strerror(errno));
		return -EINVAL;
	}

	ret = HANDLE_EINTR(ftruncate(memfd, 2 * BUFFER_SIZE));
	if (ret < 0) {
		printf("ftruncate() error: %s", strerror(errno));
		return -EINVAL;
	}

	// udmabuf_create requires that file descriptors be sealed with
	// F_SEAL_SHRINK.
	if (fcntl(memfd, F_ADD_SEALS, F_SEAL_SHRINK) < 0) {
		printf("fcntl() error: %s", strerror(errno));
		return -EINVAL;
	}

	addr = (uint8_t *)mmap(NULL, 2 * BUFFER_SIZE, PROT_WRITE | PROT_READ, MAP_SHARED, memfd, 0);
	memset(addr, 6, 2 * BUFFER_SIZE);
	munmap(addr, 2 * BUFFER_SIZE);

	create.memfd = memfd;
	create.flags = UDMABUF_FLAGS_CLOEXEC;
	create.offset = BUFFER_SIZE;
	create.size = BUFFER_SIZE;

	test->ext_mem.fd = HANDLE_EINTR(ioctl(test->udmabuf_dev, UDMABUF_CREATE, &create));
	if (test->ext_mem.fd < 0) {
		printf("error creating udmabuf");
		return -EINVAL;
	}

	close(memfd);
#elif defined(__Fuchsia__)
	// Add code here?
#endif
	test->ext_mem.created_by_vulkan = false;
	return 0;
}

static int test_gl_import(struct zero_copy_test *test)
{
	GLuint buffer_id, memobj;
	GLenum err;

	if (test->ext_mem.fd < 0) {
		printf("No external fd available\n");
		return -EINVAL;
	}

	glGenBuffers(1, &buffer_id);
	glBindBuffer(GL_ARRAY_BUFFER, buffer_id);

	// Clear the error
	err = glGetError();
	if (err != GL_NO_ERROR)
		printf("The error is %d\n", err);

	test->funcs.glCreateMemoryObjectsEXT(1, &memobj);
	test->funcs.glImportMemoryFdEXT(memobj, BUFFER_SIZE, GL_HANDLE_TYPE_OPAQUE_FD_EXT,
					test->ext_mem.fd);
	test->funcs.glBufferStorageMemEXT(GL_ARRAY_BUFFER, BUFFER_SIZE, memobj, 0);

	/*
	 * Calling glMapBufferRange(..) on an externally created file descriptor (whether
	 * created by Vulkan / sg-list) is under-defined and fails on Mesa drivers.
	 *
	 * Though mapping the buffer in theory is less important for virtualization [the
	 * guest will do the mapping].
	 */
	err = glGetError();
	if (err != GL_NO_ERROR) {
		printf("The error is %d\n", err);
		return -EINVAL;
	}

	test->ext_mem.fd = -1;
	return 0;
}

static int test_vk_import(struct zero_copy_test *test)
{
	VkResult res;
	VkDeviceMemory memory;
	int ret;
	uint8_t *addr;
	uint32_t index;
	VkExternalMemoryHandleTypeFlagBits handle_type;

	if (test->ext_mem.fd < 0) {
		printf("No external fd available\n");
		return -EINVAL;
	}

	VkMemoryFdPropertiesKHR fd_props = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR,
	};

#if defined(__linux__)
	handle_type = VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT;
#elif defined(__Fuchsia__)
	handle_type = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD;
#endif

	res = test->funcs.vkGetMemoryFdPropertiesKHR(test->dev, handle_type, test->ext_mem.fd,
						     &fd_props);

	check_vk_success(res, "vkGetMemoryFdPropertiesKHR");

	ret = find_memory_type(
	    test->phys_dev,
	    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &index,
	    fd_props.memoryTypeBits);
	if (ret < 0) {
		printf("no valid memory import  type");
		return -EINVAL;
	}

	const VkImportMemoryFdInfoKHR memory_fd_info = {
		.sType = VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR,
		.pNext = NULL,
		.handleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT,
		.fd = test->ext_mem.fd,
	};

	res = vkAllocateMemory(test->dev,
			       &(VkMemoryAllocateInfo){
				   .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
				   .pNext = &memory_fd_info,
				   .allocationSize = BUFFER_SIZE,
				   .memoryTypeIndex = index,
			       },
			       /*pAllocator*/ NULL, &memory);

	check_vk_success(res, "vkAllocateMemory");

	/*
	 * On Intel platforms atleast, vkMapMemory(..) fails for external memory created outside
	 * the Intel driver.  It could probably be fixed if someone wants to fix it.
	 *
	 * For virtualization, gfxstream will not need to map the platform-specific memory once imported.
	 * However, if a vendor wants to make vkMapMemory(..) work too, that could aide in debugging.
	 */
	if (test->ext_mem.created_by_vulkan) {
		res = vkMapMemory(test->dev, memory, 0, BUFFER_SIZE, 0, (void **)&addr);
		check_vk_success(res, "vkMapMemory");
		if (addr[1] != 6) {
			printf("Invalid buffer data");
			return -EINVAL;
		}
		vkUnmapMemory(test->dev, memory);
	}

	test->ext_mem.fd = -1;
	vkFreeMemory(test->dev, memory, /*pAllocator*/ NULL);
	return 0;
}

int main(int argc, char *argv[])
{
	struct zero_copy_test test = { 0 };
	int result;

	result = test_init_gl(&test);
	result |= test_init_vk(&test);
	result |= test_init_platform_specific(&test);

	result |= test_vk_create(&test);
	result |= test_gl_import(&test);

	result |= test_vk_create(&test);
	result |= test_vk_import(&test);

	result |= test_create_from_sglist(&test);
	result |= test_gl_import(&test);

	result |= test_create_from_sglist(&test);
	result |= test_vk_import(&test);

	if (result < 0) {
		printf("[  FAILED  ] zero_copy_test failed\n");
		return result;
	}

	printf("[  PASSED  ] zero_copy_test success\n");
	return 0;
}
